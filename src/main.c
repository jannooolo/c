/*  Copyright (C) 2016 Janno Oolo <Janno.oolo@gmail.com>
*
*   This file is part of I237 Lab06.
*
*   I237 Lab06 is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   I237 Lab06 is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with I237 Lab06.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <string.h>
#include  <util/atomic.h>
#include "uart-wrapper.h"
#include "print_helper.h"
#include "hmi_msg.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "rfid.h"
#define BAUD 9600
#define BLINK_DELAY_MS 100
#define UART_STATUS_MASK    0x00FF


volatile uint32_t time;


static inline void init_system_clock(void)
{
    TCCR5A = 0;
    TCCR5B = 0;
    TCCR5B |= _BV(WGM52) | _BV(CS52);
    OCR5A = 62549;
    TIMSK5 |= _BV(OCIE5A);
}


static inline void init_rfid_reader(void)
{
    /* Init RFID-­‐RC522 */
    MFRC522_init();
    PCD_Init();
}


static inline void handle_door()
{
    Uid uid;
    card_t card;
    uint32_t cur_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        cur_time = time;
    }
    static uint32_t message_start;
    static uint32_t door_open_start;

    if (PICC_IsNewCardPresent()) {
        PICC_ReadCardSerial(&uid);
        card.uid_size = uid.size;
        memcpy(&card.uid, &uid.uidByte, uid.size);
        card.user = NULL;
        card_t *found_card = rfid_find_card(&card);

        if (found_card) {
            lcd_goto(0x40);
            lcd_puts(found_card->user);

            for (int8_t i = 16 - strlen(found_card->user); i > -1; i--) {
                lcd_putc(' ');
            }

            PORTA |= _BV(PORTA1);
        } else {
            PORTA &= ~_BV(PORTA1);
            lcd_goto(0x40);
            lcd_puts_P(access_denied_msg);

            for (int8_t i = 16 - strlen_P(access_denied_msg); i > -1; i--) {
                lcd_putc(' ');
            }
        }

        door_open_start = cur_time;
        message_start = cur_time;
    }

    if ((message_start + 5) < cur_time) {
        message_start = cur_time + 7;
        lcd_goto(0x40);

        for (int8_t i = 16; i > -1; i--) {
            lcd_putc(' ');
        }
    }

    if ((door_open_start + 2) < cur_time) {
        PORTA &= ~_BV(PORTA1);
    }
}


static inline void init()
{
    DDRA |= _BV(DDA3);
    DDRA |= _BV(DDA1);
    init_system_clock();
    lcd_init();
    lcd_clrscr();
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    sei();
    stdin = stdout = &uart0_io;
    stderr = &uart3_out;
}


static inline void heartbeat()
{
    static uint32_t last_time;
    uint32_t cur_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        cur_time = time;
    }

    if ((last_time - cur_time) > 0) {
        PORTA ^= _BV(PORTA3);
        fprintf_P(stderr, PSTR(UPTIME "\n"), cur_time);
    }

    last_time = cur_time;
}


static inline void print_info()
{
    fprintf_P(stdout, PSTR(STUD_NAME "\n"));
    lcd_puts_P(PSTR(STUD_NAME));
}


int main (void)
{
    init();
    init_rfid_reader();
    version(stderr);
    print_info();
    //Create microrl object and pointer on it
    microrl_t rl;
    microrl_t *prl = &rl;
    // Call init with ptr to microrl instance and print callback
    microrl_init(prl, cli_print);
    //Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);

    while (1) {
        heartbeat();
        handle_door();
        //CLI commands are handled in cli_execute()
        microrl_insert_char (prl, cli_get_char());
        //months();/*It was before led on and blinked turned on and off after letter*/
    }
}


/* System clock ISR */
ISR(TIMER5_COMPA_vect)
{
    time++;
}

# Arduino Mega USB UART converter wiring

## Introduction

This wiring schema uses only Tx from Arduino and is suitable to be used as standard error console.

## Wiring illustration

![arduino-mega-usb-uart-wiring.png](arduino-mega-usb-uart-wiring.png)

## Wiring table

| Signal | ATMega2560 port and pin | Arduino Mega 2560 pin | USB UART converter pin |
| --- | --- | --- | --- |
<<<<<<< HEAD
| Ground (GND) | - | GND | GND |
| Transmit data  from Arduino (TxD) | PORTJ 1 (TXD3) | 14 (TX3) | TxD |
=======
| Ground (GND) | GND | GND | GND |
<<<<<<< HEAD:doc/Arduino-Mega-USB-UART-wiring.markdown
| Transmit data  from Arduino (TxD) | PORTJ pin 1 (TXD3) | Digital pin 14 (TX3) | TxD |
>>>>>>> 40c0ac68886b1a59597fc3021891b32b190c0531
=======
| Transmit data  from Arduino (TxD) | PORTJ pin 1 (TXD3) | Digital pin 14 (TX3) | RxD |
>>>>>>> d6fdf0ac12c50c306926f19f4fc28ce2328e6113:doc/arduino-mega-usb-uart-wiring.markdown

